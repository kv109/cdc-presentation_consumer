require 'rails_helper'

RSpec.describe NotesController, :type => :request do

  it 'GET show' do
    body = '{"title":"Qui nulla eveniet nemo quia"}'
    stub_request_to_api_with(body)

    get '/my_notes/1'

    html = '<td>Qui nulla eveniet nemo quia</td>'
    expect(response.body).to match(html)
  end

  def stub_request_to_api_with(body)
    stub_request(:any, Regexp.new('http://api.org/')).
      to_return(status: 200, body: body)
  end

end


